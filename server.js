var express = require('express');
var app = express();
var port = 3000;

app.get('/',(req,res) => {

    res.sendFile(__dirname + '/index.html');
})

app.listen(port,() => {console.log(`Server is listening on port: ${port}`)});
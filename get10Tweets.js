var express = require('express');
var request = require('request');
var url = require('url');
var app = express();
var port = 3000;

app.get('/tweets/:username', (req, response) => {
    var username = req.params.username;
    options = {
        protocol: 'http:',
        host: 'api.twitter.com',
        path:'/1/statuses/user_timeline.json',
        query:{ screen_name:username,count:10}
    }
    var twitterUrl = url.format(options);
    request(twitterUrl).pipe(response);
})

app.listen(port, () => { console.log(`Server is listening on port: ${port}`) });